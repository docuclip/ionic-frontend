import React, { useState, useEffect } from "react"
import { IonButtons, IonButton, IonContent, IonMenuButton, IonPage } from "@ionic/react"
import { Capacitor } from '@capacitor/core';

import "./Toolbar.css"
import MenuButton from "./assets/hamburger.png"
import Tabs from "../components/Tabs"

const MenuTemplate: React.FC<{
  name: string
  component: any
  tabs?: boolean
}> = (props) => {
  const [isIOSButtonVisible, setIsIOSButtonVisible] = useState<boolean>(false);

  useEffect(() => {
    if (Capacitor.getPlatform() === 'ios') {
      setIsIOSButtonVisible(true)
    }
  }, []);


  const classNameValue = props.tabs ? 'header-margin-tabs main-container' : 'header-margin main-container'
  return (
    <IonPage>
      <IonContent>
        <div className='toolbar'>
          <IonButtons slot='start'>
            <IonMenuButton className='menu-button'>
              <img src={MenuButton} alt='menu' />
            </IonMenuButton>
          </IonButtons>
          <p className='toolbar-header'>{props.name}</p>
          <p style={{ color: "grey", fontSize: "10px", margin: 'auto 0 10px auto' }}>beta_22</p>
          {isIOSButtonVisible && <IonButton color="primary" onClick={() => window.location.href = '/'}>Home</IonButton>}
        </div>
        {props.tabs ? <div className="toolbar-tabs"><Tabs /><div className='toolbar-after-tabs'></div></div> : <div className='toolbar-normal-tabs'></div>}
        <div className={classNameValue}>{props.component}</div>
      </IonContent>
    </IonPage>
  )
}

export default MenuTemplate
