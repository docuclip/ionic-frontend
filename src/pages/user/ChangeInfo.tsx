import {
  IonAlert,
  IonButton,
  IonInput,
  IonItem,
  IonLabel,
  IonLoading,
  IonRow,
} from "@ionic/react"
import axios from "axios"
import React from "react"
import { destroyToken, getToken, isLoggedIn, setToken } from "../../components/login/TokenProvider"
import apiRoutes from "../../components/Routes"

import "./profile.css"

type props = {}
type states = {
  username: string
  email: string
  fullname: string
  nickname: string

  error: string
  isLoading: boolean
}

export default class ChangeInfo extends React.Component<props, states> {
  constructor(props: props) {
    super(props)

    if (! isLoggedIn()) {
      window.location.replace('/')
    }

    const { userEmail, userName, fullname, nickname } = getToken()

    this.state = {
      username: userName,
      email: userEmail,
      fullname: fullname,
      nickname: nickname,

      error: "",
      isLoading: false,
    }
  }

  submitClickHandler = () => {
    this.setState({ isLoading: true })

    // TODO: implement update user info
    // 1. Add field validation (tick)
    // 2. Update fields and take back to profile view (tick)
    // 3. Remove old token and get new token in its place (untick)

    // validation
    if (!this.state.username || !this.state.email || !this.state.fullname || !this.state.nickname) {
      this.setState({ error: "None of the fields can be empty", isLoading: false })
      return
    }

    // update fields
    let userData = new FormData()
    const userId = getToken().userId
    userData.append("id", userId)
    userData.append("username", this.state.username)
    userData.append("email", this.state.email)
    userData.append("fullname", this.state.fullname)
    userData.append("nickname", this.state.nickname)

    axios
      .post(apiRoutes.profile.updateDetails, userData)
      .then((response) => {
        console.log('response.data: ', response.data);
        if (response.data.status === 'success') {
          // destroy the previous token and create a new one in its place
          destroyToken()

          const tokenData = {
            "status": "success",
            "userId": userId,
            "userEmail": this.state.email,
            "userName": this.state.username,
            "fullname": this.state.fullname,
            "nickname": this.state.nickname,
            "jwtToken": ""
          }
          setToken(tokenData)

          window.location.replace('/profile')
        } else {
          this.setState({ error: response.data.error })
        }
      })
      .catch((error) => {
        this.setState({ error: "An error occurred while contacting the server. Please try again after a while.", isLoading: false })
        console.log("update error: ", error)
      })

    // if (this.state.newPassword === this.state.newPasswordRepeat) {
    //   const { userName } = getToken()

    //   let credentialData = new FormData()
    //   credentialData.append("username", userName)
    //   credentialData.append("password", this.state.oldPassword)
    //   axios
    //     .post(apiRoutes.login, credentialData)
    //     .then((response) => {
    //       if (response.data.status !== "success") {
    //         this.setState({
    //           isLoading: false,
    //           error: "Your old password is incorrect",
    //         })
    //       }
    //     })
    //     .catch((error) => {
    //       this.setState({
    //         isLoading: false,
    //         error: error,
    //       })
    //     })
    //     .then(() => {
    //       const { userId } = getToken()

    //       const passwordData = new FormData()
    //       passwordData.append('userId', userId)
    //       passwordData.append('newPassword', this.state.newPassword)
    //       axios.post(apiRoutes.changePassword, passwordData)
    //         .then((response) => {
    //           if (response.data.status === 'success') {
    //             window.location.replace('/profile')
    //           }
    //         }).catch((error) => {
    //           console.log('error: ', error);
    //           this.setState({
    //             isLoading: false,
    //             error: "There was an internal error"
    //           })
    //         })
    //     })
    // } else this.setState({ error: "New passwords do not match" })

    this.setState({ isLoading: false })
  }

  onChangeHandler = (event: any) => {
    const name: string = event.target.name
    const value: string = event.target.value

    this.setState({ [name]: value } as any)
  }

  render() {
    const inputFieldStyle = {
      width: "100%",
    }

    return (
      <React.Fragment>
        <IonAlert
          isOpen={!!this.state.error}
          message={this.state.error}
          buttons={[
            {
              text: "Okay",
              handler: () => {
                this.setState({ error: "" })
              },
            },
          ]}
        />

        <IonLoading
          isOpen={this.state.isLoading}
          onDidDismiss={() => this.setState({ isLoading: false })}
          message={"Please wait..."}
        />

        <IonRow>
          <IonItem style={inputFieldStyle}>
            <IonLabel position='floating'>Username</IonLabel>
            <IonInput
              type='text'
              name='username'
              value={this.state.username}
              onIonChange={this.onChangeHandler}
            />
          </IonItem>

          <IonItem style={inputFieldStyle}>
            <IonLabel position='floating'>Email</IonLabel>
            <IonInput
              type='text'
              name='email'
              value={this.state.email}
              onIonChange={this.onChangeHandler}
            />
          </IonItem>

          <IonItem style={inputFieldStyle}>
            <IonLabel position='floating'>Fullname</IonLabel>
            <IonInput
              type='text'
              name='fullname'
              value={this.state.fullname}
              onIonChange={this.onChangeHandler}
            />
          </IonItem>

          <IonItem style={inputFieldStyle}>
            <IonLabel position='floating'>Nickname</IonLabel>
            <IonInput
              type='text'
              name='nickname'
              value={this.state.nickname}
              onIonChange={this.onChangeHandler}
            />
          </IonItem>

          <IonButton style={inputFieldStyle} onClick={this.submitClickHandler}>
            Update Info
          </IonButton>
        </IonRow>
      </React.Fragment>
    )
  }
}
