import React from "react"

import "./profile.css"

type details = {
  userId: string
  userEmail: string
  userName: string
  fullname: string
  nickname: string
}

type props = { details: details }
type states = {}

export default class UserDetails extends React.Component<props, states> {
  render() {
    return (
      <div style={{ borderBottom: "1px solid #ccc" }}>
        <h5 className='profile-title'>User Details</h5>
        <div className='singleFile-card-item'>
          <span>Phone No</span>
          <p>{this.props.details.userName}</p>
        </div>

        <div className='singleFile-card-item'>
          <span>Email Address</span>
          <p>{this.props.details.userEmail}</p>
        </div>

        <div className='singleFile-card-item'>
          <span>Fullname</span>
          <p>{this.props.details.fullname}</p>
        </div>

        <div className='singleFile-card-item'>
          <span>Nickname</span>
          <p>{this.props.details.nickname}</p>
        </div>
      </div>
    )
  }
}
