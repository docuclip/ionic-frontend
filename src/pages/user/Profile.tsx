import { IonButton } from "@ionic/react"
import React from "react"
import { getToken, isLoggedIn } from "../../components/login/TokenProvider"

import "./profile.css"
import UserDetails from "./UserDetails"

type props = {}

type details = {
  userId: string
  userEmail: string
  userName: string
  fullname: string
  nickname: string
}
type states = {
  details: details
}

export default class Profile extends React.Component<props, states> {
  constructor(props: props) {
    super(props)

    if (!isLoggedIn() || getToken() === undefined) {
      window.location.replace("/")
    }

    const { userId, userEmail, userName, fullname, nickname } = getToken()

    this.state = {
      details: {
        userId: userId,
        userEmail: userEmail,
        userName: userName,
        fullname: fullname,
        nickname: nickname
      },
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className='profile-card-content'>
          <UserDetails details={this.state.details} />
        </div>
        <IonButton expand='block' onClick={() => window.location.href = '/profile/change-info'}>
          Edit Info
        </IonButton>
      </React.Fragment>
    )
  }
}
