import React, { useState, useRef } from "react"
import {
  IonContent,
  IonItem,
  IonLabel,
  IonInput,
  IonAlert,
  IonLoading,
} from "@ionic/react"
import { Link } from "react-router-dom"

// assets import
import "./Login.css"
import Logo from "./Logo"
import backgroundImage from "./background.png"

// axios and routes import
import axios from "axios"
import apiRoutes from "../../components/Routes"

// import login modules
import { isLoggedIn } from "../../components/login/TokenProvider"

// if user is already logged in, then redirect to home
const redirectIfLoggedin = () => {
  if (isLoggedIn() === true) {
    return window.location.replace("/category")
  }
}

const Login: React.FC = () => {
  redirectIfLoggedin()

  const [error, setError] = useState<string>()
  const [isLoading, setIsLoading] = useState<boolean>(false)

  // declare refs
  const enteredUsernameRef = useRef<HTMLIonInputElement>(null)

  const loginClickHandler = () => {
    setIsLoading(true)

    // get the values from refs
    const username = enteredUsernameRef.current?.value

    // throw and error if either of the fields are empty
    if (!username) {
      setError("Please enter the phone number")
      setIsLoading(false)
      return
    }

    const OTP = (Math.floor(Math.random() * 10000) + 10000)
      .toString()
      .substring(1)
    // pack data to be sent in the post request and call the api
    let credentialData = new FormData()
    credentialData.append("username", username as string)
    credentialData.append("otp", OTP)
    axios
      .post(apiRoutes.sendOtp, credentialData)
      .then((response) => {
        setIsLoading(false)
        if (response.data.status === "success") {
          sessionStorage.setItem('otp', OTP)
          sessionStorage.setItem('username', username as string)
          window.location.href = '/otp'
        } else {
          if (response.data.error) {
            setError(response.data.error)
          } else {
            setError("Invalid credentials")
          }
        }
      })
      .catch((error) => {
        setIsLoading(false)
        setError("Server unreachable: " + error)
      })
      .then(() => {
        setIsLoading(false)
      })

    // // pack data to be sent in the post request and call the api
    // let credentialData = new FormData();
    // credentialData.append("username", username as string);
    // credentialData.append("password", password as string);
    // axios
    //   .post(apiRoutes.login, credentialData)
    //   .then((response) => {
    //     setIsLoading(false);
    //     console.log('response.data: ', response.data);
    //     if (response.data.status === "success") {
    //       // set user data to local storage
    //       setToken(response.data);
    //       redirectIfLoggedin();
    //     } else {
    //       if (response.data.error) {
    //         setError(response.data.error)
    //       } else {
    //         setError("Invalid credentials");
    //       }
    //     }
    //   })
    //   .catch((error) => {
    //     setIsLoading(false);
    //     setError("Server unreachable: " + error);
    //   })
    //   .then(() => {
    //     setIsLoading(false);
    //   })
  }

  return (
    <React.Fragment>
      <IonAlert
        isOpen={!!error}
        message={error}
        buttons={[
          {
            text: "Okay",
            handler: () => {
              setError("")
            },
          },
        ]}
      />

      <IonLoading
        isOpen={!!isLoading}
        onDidDismiss={() => setIsLoading(false)}
        message={"Please wait..."}
      />

      <IonContent>
        <div className='main-container'>
          <div className='row-vertical'>
            <Logo />
          </div>
        </div>
        <div className='row-vertical login-background-image'>
          <img src={backgroundImage} alt='welcome' />
        </div>
        <div className='main-container'>
          <div className='row-vertical login-main-panel'>
            <div className='row-vertical login-title'>
              <b>Login to Docuclip</b>
            </div>
            <div className='row-vertical muted'></div>
            <IonItem className='input-form-group'>
              <IonLabel position='floating'>Phone</IonLabel>
              <IonInput
                type='text'
                autocomplete='username'
                ref={enteredUsernameRef}
              />
            </IonItem>
            <div className='row-vertical'>
              <button
                className='center custom-button'
                onClick={loginClickHandler}
              >
                Login
              </button>
            </div>
            <div className='row-vertical text-center'>
              <p>
                New user? <Link to='register'>Sign up</Link>
              </p>
            </div>
          </div>
        </div>
      </IonContent>
    </React.Fragment>
  )
}

export default Login
