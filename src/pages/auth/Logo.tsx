import React from "react"
import LogoImage from './logo.png'

const Logo: React.FC = () => {
  return (
    <img src={LogoImage} alt="Docuclip" width={175} style={{ marginTop: 25 }} />
  )
}

export default Logo
