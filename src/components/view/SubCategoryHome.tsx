import { IonAlert, IonLoading } from "@ionic/react"
import Axios from "axios"
import React from "react"
import MenuTemplate from "../../pages/Toolbar"
import FilesListHandler from "../../pages/view/FilesListHandler"
import apiRoutes from "../Routes"
import CategoryItem from "./CategoryItem"

// import network plugin
import {
  Plugins,
  FilesystemDirectory,
  FilesystemEncoding,
} from "@capacitor/core"

type props = {
  match: { params: { urlSafeCategory?: string } }
}
type states = {
  isLoading: boolean
  categories: any
  subCategories: any
  selectedCategory: string
  isEmpty: boolean
  isStructureNotFound: boolean
}

type SubCategoryResponse = {
  categories: Array<string>
  additionalFields: Object
}

class SubCategoryHome extends React.Component<props, states> {
  constructor(props: props) {
    super(props)

    const title = this.props.match.params.urlSafeCategory
    let selectedCategory: string = ""

    // if title doesn't exist, then redirect to home - shouldn't happen, just an edge case
    if (title) {
      const titleArray = title.split("_")
      for (var i = 0; i < titleArray.length; i++) {
        titleArray[i] = titleArray[i][0].toUpperCase() + titleArray[i].slice(1)
      }
      selectedCategory = titleArray.join(" ")
    }

    this.state = {
      isLoading: true,
      categories: null,
      selectedCategory: selectedCategory,
      isEmpty: false,
      subCategories: [],
      isStructureNotFound: false
    }
  }

  async getCategoryStructure(): Promise<SubCategoryResponse> {
    const { Network } = Plugins
    let status = await Network.getStatus()

    if (status.connected === false) {
      const { Filesystem } = Plugins

      let response = await Filesystem.readFile({
        path: "structure.txt",
        directory: FilesystemDirectory.Data,
        encoding: FilesystemEncoding.UTF8,
      })
      .catch((error) => {
        console.log(error)
        this.setState({ isStructureNotFound: true, isLoading: false })

        return {data: ''}
      })

      return JSON.parse(response.data)
    } else {
      const postResponse = await Axios.post(apiRoutes.getCategories).then(
        async (response) => {
          const { Filesystem } = Plugins

          try {
            await Filesystem.writeFile({
              path: "structure.txt",
              data: JSON.stringify(response.data),
              directory: FilesystemDirectory.Data,
              encoding: FilesystemEncoding.UTF8,
            })
          } catch (e) {
            console.error("Unable to write file", e)
          }

          return response.data
        }
      )

      return postResponse
    }
  }

  async componentDidMount() {
    this.getCategoryStructure()
      .then((data) => {
        // get all latest categories from additionalFields
        const categories: any = []
        for (const property in data.additionalFields) {
          if (
            Object.prototype.hasOwnProperty.call(
              data.additionalFields,
              property
            )
          ) {
            categories.push(data.additionalFields[property].title)
          }
        }

        // set the categories and additionalFields as an entire object to this component's state
        this.setState({ categories: categories })
        return data.additionalFields
      })
      .then((additionalFields) => {
        let subCategories: any = []
        // retrieve the sub category
        for (const property in additionalFields) {
          if (
            Object.prototype.hasOwnProperty.call(additionalFields, property)
          ) {
            if (
              additionalFields[property].title.toLowerCase() ===
              this.state.selectedCategory.toLowerCase()
            ) {
              subCategories = additionalFields[property].subCategory
            }
          }
        }

        return subCategories
      })
      .then((subCategories) => {
        if (
          Object.keys(subCategories).length === 0 ||
          subCategories.hasOwnProperty("category")
        ) {
          this.setState({ isEmpty: true })
        } else {
          let subCategoryInfo = []
          for (const subCategoryTitle in subCategories) {
            subCategoryInfo.push({
              title: subCategories[subCategoryTitle].title,
            })
          }
          this.setState({ subCategories: subCategoryInfo })
        }
      })
      .then(() => this.setState({ isLoading: false }))
  }

  render() {
    return (
      <React.Fragment>
        <IonLoading
          isOpen={this.state.isLoading}
          onDidDismiss={() => this.setState({ isLoading: false })}
          message={"Please wait..."}
        />

        <IonAlert
          isOpen={this.state.isStructureNotFound}
          message="Structure data not found. Please connect to the internet and open the category to download the structure"
          buttons={[
            {
              text: "Okay",
              handler: () => {
                this.setState({ isStructureNotFound: false })
                window.location.replace('/category')
              },
            },
          ]}
          onDidDismiss={() => {
            this.setState({ isStructureNotFound: false })
            window.location.replace('/category')
          }}
        />

        <MenuTemplate
          name={this.state.selectedCategory}
          component={
            <React.Fragment>
              {this.state.isEmpty ? (
                <FilesListHandler title={this.state.selectedCategory} />
              ) : (
                <div className='category-slots'>
                  {this.state.subCategories.map(
                    (category: any, index: number) => {
                      return <CategoryItem key={index} title={category.title} />
                    }
                  )}
                </div>
              )}
            </React.Fragment>
          }
        />
      </React.Fragment>
    )
  }
}

export default SubCategoryHome
