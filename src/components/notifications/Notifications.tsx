import {
  FilesystemDirectory,
  FilesystemEncoding,
  Plugins,
} from "@capacitor/core"
import Axios from "axios"
import React from "react"
import { getToken, isLoggedIn } from "../login/TokenProvider"
import apiRoutes from "../Routes"

import NotificationEntry from "./NotificationEntry"

type notifications = {
  id: string
  text: string
  action: string
  public_name: string
}

type props = {}
type states = {
  notifications: Array<notifications> | null
  isStructureNotFound: boolean
  subCategoryStructure: any
}

type SubCategoryResponse = {
  categories: Array<string>
  additionalFields: Object
}

class Notifications extends React.Component<props, states> {
  constructor(props: props) {
    super(props)

    if (!isLoggedIn()) window.location.replace("/")

    this.state = {
      notifications: null,
      isStructureNotFound: false,
      subCategoryStructure: null,
    }
  }

  async componentDidMount() {
    await this.getCategoryStructure()
      .then((data) => data.additionalFields)
      .then((additionalFields) => {
        let subCategories: any = {}
        // retrieve the sub category
        for (const firstProperty in additionalFields) {
          for (const secondProperty in additionalFields[firstProperty][
            "subCategory"
          ]) {
            if (secondProperty === "category") continue
            subCategories[secondProperty] =
              additionalFields[firstProperty]["subCategory"][secondProperty][
                "title"
              ]
          }
        }

        this.setState({ subCategoryStructure: subCategories })
      })

    const formData = new FormData()
    formData.append("userId", getToken().userId)
    Axios.post(apiRoutes.notifications.get, formData)
      .then((response) => {
        if (response.data.status === "success") {
          this.setState({ notifications: response.data.notifications })
        }
      })
      .catch((error) => {
        console.log("error: ", error)
      })
  }

  async getCategoryStructure(): Promise<SubCategoryResponse> {
    const { Network } = Plugins
    let status = await Network.getStatus()

    if (status.connected === false) {
      const { Filesystem } = Plugins

      let response = await Filesystem.readFile({
        path: "structure.txt",
        directory: FilesystemDirectory.Data,
        encoding: FilesystemEncoding.UTF8,
      }).catch((error) => {
        console.log(error)
        this.setState({ isStructureNotFound: true })
        return { data: "" }
      })

      return JSON.parse(response.data)
    } else {
      const postResponse = await Axios.post(apiRoutes.getCategories).then(
        async (response) => {
          const { Filesystem } = Plugins

          try {
            await Filesystem.writeFile({
              path: "structure.txt",
              data: JSON.stringify(response.data),
              directory: FilesystemDirectory.Data,
              encoding: FilesystemEncoding.UTF8,
            })
          } catch (e) {
            console.error("Unable to write file", e)
          }

          return response.data
        }
      )

      return postResponse
    }
  }

  render() {
    return (
      <div className='container'>
        <div
          style={{
            zIndex: -99,
            textAlign: "center",
            position: "fixed",
            width: "85%",
          }}
        >
          No Notifications
        </div>
        {this.state.notifications
          ? this.state.notifications.map((notification: notifications) => {
              return (
                <NotificationEntry
                  key={notification.id}
                  id={notification.id}
                  action={notification.action}
                  text={notification.text}
                  public_name={notification.public_name}
                // subCategoryStructure={this.state.subCategoryStructure}  //not required
                />
              )
            })
          : null}
      </div>
    )
  }
}

export default Notifications
