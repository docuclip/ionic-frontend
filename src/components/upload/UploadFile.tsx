import React from "react";
import { IonRow, IonCol, IonButton } from "@ionic/react";

// import axios and api routes
import axios from "axios";
import apiRoutes from "../Routes";

// import auth functions
import { getToken } from "../login/TokenProvider";

type props = {
  parentSetState: any;
};
type states = {
  file: any;
  filename: string;
};

class UploadFile extends React.Component<props, states> {
  constructor(props: props) {
    super(props);

    this.state = {
      file: null,
      filename: "Select a file...",
    };
  }

  // call this function when upload is clicked
  uploadClickHandler = () => {
    // show the loading icon from parent state
    this.props.parentSetState("isLoading", true);

    // if no file is selected, cancel loading icon and show error
    if (!this.state.file) {
      this.props.parentSetState("isLoading", false);
      this.props.parentSetState("error", "Please select a file to upload");
      return;
    }

    // pack the data, that is the current user's ID and the file being uploaded
    // and make and api post call
    const data = new FormData();
    data.append("file", this.state.file);
    data.append("userId", getToken().userId);
    axios
      .post(apiRoutes.upload, data)
      .then((response) => {
        if (response.data.status === "success") {
          // set the uploaded flag to true to show the view to add metadata to the file
          // and then set response data to parent component
          this.props.parentSetState("responseData", response.data);
          this.props.parentSetState("isUploaded", true);
        } else {
          this.props.parentSetState("error", response.data.error);
        }

        this.props.parentSetState("isLoading", false);
      })
      .catch((error) => {
        this.props.parentSetState("error", error);
      });
  };

  // when the file upload input has selected a file, then this function updates the
  // file path on the state
  uploadChangeHandler = (event: any) => {
    this.setState({
      file: event.target.files[0],
      filename: event.target.files[0].name,
    });
  };

  render() {
    return (
      <div className="vertically-center">
        <IonRow>
          <IonCol>
            <div>
              <label className="custom-file-label" htmlFor="upload-input">
                {this.state.filename}
              </label>
              <input
                type="file"
                name="file"
                id="upload-input"
                className="custom-file-input"
                onChange={this.uploadChangeHandler}
              />
            </div>
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol>
            <IonButton expand="block" onClick={this.uploadClickHandler}>
              Upload
            </IonButton>
          </IonCol>
        </IonRow>
      </div>
    );
  }
}

export default UploadFile;
