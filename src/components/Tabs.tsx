import React from "react"
import { IonBadge, IonIcon, IonTabButton } from "@ionic/react"
import {
  calendar,
  cloudUpload,
  notifications,
  qrCode,
  scan,
} from "ionicons/icons"

import { getToken, isLoggedIn } from "./login/TokenProvider"
import apiRoutes from "./Routes"
import Axios from "axios"

type props = {}
type states = { notificationsCount: number }

class Tabs extends React.Component<props, states> {
  constructor(props: props) {
    super(props)

    this.state = {
      notificationsCount: 0,
    }
  }

  componentDidMount() {
    if (isLoggedIn()) {
      const formData = new FormData()
      formData.append("userId", getToken().userId)
      Axios.post(apiRoutes.notifications.get, formData)
        .then((response) => {
          if (response.data.status === "success") {
            this.setState({ notificationsCount: response.data.count })
          }
        })
        .catch((error) => {
          console.log("error: ", error)
        })
    }
  }
  render() {
    return (
      <div className='menu-tabs'>
        <IonTabButton className='menu-tabs-icon' href='/calendar'>
          <IonIcon icon={calendar} />
        </IonTabButton>

        <IonTabButton className='menu-tabs-icon' href='/upload'>
          <IonIcon icon={cloudUpload} />
        </IonTabButton>

        <IonTabButton className='menu-tabs-icon' href='/scanner'>
          <IonIcon icon={qrCode} />
        </IonTabButton>

        <IonTabButton className='menu-tabs-icon' href='/category'>
          <IonIcon icon={scan} />
        </IonTabButton>

        <IonTabButton className='menu-tabs-icon' href='/notifications'>
          <IonBadge color='danger'>{this.state.notificationsCount}</IonBadge>
          <IonIcon icon={notifications} />
        </IonTabButton>
      </div>
    )
  }
}

export default Tabs
